import { useContext } from "react";
import { NavLink, useHistory } from 'react-router-dom'
import { Context } from '../../context/Context'

const NavbarFront = () => {
    const { user, dispatch} = useContext(Context);
    const history = useHistory();

    const LogoutTask = () =>{
        dispatch({ type: "LOGOUT" });
        history.push('/');
    }

    return(
        <nav className="navbar navbar-expand-md bg-dark navbar-dark">
            <NavLink to="/" className="navbar-brand">OFFEE SHOP </NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="collapsibleNavbar">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link">HOME</NavLink>
                    </li>

                    {user 
                    && (
                        <>
                            <li className="nav-item">
                                <NavLink to="/category" className="nav-link">Category</NavLink>
                            </li>
                        </>
                        ) 
                    }
                    
                </ul>
            </div>  
            
            <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                <ul className="navbar-nav ml-auto">
                    {user ? 
                            (
                                <>

                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                            {user.name}
                                        </a>
                                        <div className="dropdown-menu">
                                            <a className="dropdown-item" href="#">{user.email}</a>
                                            <NavLink to="#" className="dropdown-item" onClick={LogoutTask} >LOGOUT</NavLink>
                                        </div>
                                    </li>
                                </>
                            ) : (
                                <>
                                    <li className="nav-item">
                                        <NavLink to="/sign-up" className="nav-link">SING-UP</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to="/sign-in" className="nav-link">SING-IN</NavLink>
                                    </li>
                                </>
                            )
                        }
                </ul>
            </div>

        </nav>
    )
}

export default NavbarFront