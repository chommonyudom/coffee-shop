import NavbarFront from './components/navbar/NavbarFront';
import { Switch, Route } from 'react-router-dom';
import { useEffect } from "react";

import HomePage from './pages/HomePage';
import SignUp from './pages/SignUp';
import SignIn from './pages/SignIn';
import Category from './pages/Category';
import CategoryEdit from './pages/CategoryEdit';

function App() {

	return (
		<>
		<NavbarFront />
		
		<div className="container-fluid">
			<Switch>
				<Route exact path="/" component={HomePage} />
				<Route exact path="/sign-up" component={SignUp} />
				<Route exact path="/sign-in" component={SignIn} />
				<Route exact path="/category" component={Category} />
				<Route exact path="/category/:id" component={CategoryEdit} />
			</Switch>
		</div>

		</>
	);
}

export default App;
